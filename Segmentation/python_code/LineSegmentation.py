#Command to divide the pdf into words (while you are in the lines folder page): pdftoppm ../input.pdf outputname -png -rx 300 -ry 300 -f 1 -l 20

#!/usr/bin/env python
import cv2
import glob
import numpy as np
import matplotlib.pyplot as plt
import array

#You need to manually set this function according to your directory
def getImageLocations():
    return glob.glob("/home/amr/Downloads/College/Thesis-ADVIC/advic/Segmentation/Dataset/Lines/*.png")
def getThresholdImage(imageLocation):
    image = cv2.imread(imageLocation)
    height, width, depth = image.shape
    # image = cv2.resize(image,(int(width/2),int(height/2)))
    #grayscale
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    #cv2.imshow('gray',gray)
    #cv2.waitKey(0)

    #binary
    ret,thresh = cv2.threshold(gray,170,255,cv2.THRESH_BINARY_INV)

    white = np.count_nonzero(thresh)
    black = thresh.size - white

    if black < white:
        thresh = np.invert(thresh)
    return image,thresh
def getDilatedImage(thresh):
    #dilation
    # this kernel 1x3 for line detection
     kernel = np.ones((1,6), np.uint8)
    # # this kernel for paragraph segmentation and block segmentation
    # # kernel = np.ones((5,5), np.uint8)
    #
     img_dilation = cv2.dilate(thresh, kernel, iterations=3)
     kernel = np.ones((1,10), np.uint8)
     img_dilation = cv2.erode(img_dilation,kernel, iterations=3)
     kernel = np.ones((2,10), np.uint8)
     img_dilation = cv2.dilate(img_dilation,kernel, iterations=6)
     kernel = np.ones((6,3), np.uint8)
     img_dilation = cv2.dilate(img_dilation,kernel, iterations=2)
     # cv2.imshow('dilated',img_dilation)
     # cv2.waitKey(0)
     # cv2.destroyWindow('dilated')
     return img_dilation
def sort_contours(cnts, method="top-to-bottom"):
	# initialize the reverse flag and sort index
	reverse = False
	i = 0

	# handle if we need to sort in reverse
	if method == "right-to-left" or method == "bottom-to-top":
		reverse = True

	# handle if we are sorting against the y-coordinate rather than
	# the x-coordinate of the bounding box
	if method == "top-to-bottom" or method == "bottom-to-top":
		i = 1

	# construct the list of bounding boxes and sort them from top to
	# bottom
	boundingBoxes = [cv2.boundingRect(c) for c in cnts]
	(cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
		key=lambda b:b[1][i], reverse=reverse))

	# return the list of sorted contours and bounding boxes
	return (cnts, boundingBoxes)
def getLinesInImage(img_dilation,image,linesInImage):
    #find contours
    img_contours = img_dilation.copy()
    ctrs, hier = cv2.findContours(img_contours, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    copyOriginalImage = image.copy()
    sortedCtrs, BBoxs = sort_contours(ctrs)
    # cv2.imshow('In find',copyOriginalImage)
    # cv2.waitKey(0)
    # cv2.imshow('In find2',image)
    # cv2.waitKey(0)
    for i, ctr in enumerate(sortedCtrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)
        #We decrease the height of the contour because the dilates increases it
        #img2 = cv2.rectangle(image, (x,y-int(h*0.5)), (x+w, y+h-int(0.05*h)), (255,255,0), 2)
        startY = y-int(h*0.15)
        endY = y+h-int(0.05*h)
        img2 = image[startY:endY,x:x+w]
        linesInImage.append(img2)
        cv2.rectangle(copyOriginalImage,(x,startY),( x + w, endY),(90,0,255),2)
def getWordsInLine(lineImage,image,wordsInLine):
    grayLineImage = cv2.cvtColor(lineImage,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(grayLineImage,170,255,cv2.THRESH_BINARY_INV)

    #Kernel to connect the dots with the words
    kernel = np.ones((8,1), np.uint8)
    img_dilation = cv2.dilate(thresh,kernel, iterations=3)

    #Kernel to connect the word itself together if it has an isolated character like alef in the middle
    kernel = np.ones((1,3), np.uint8)
    img_dilation = cv2.dilate(img_dilation,kernel, iterations=4)

    # cv2.imshow('dilated', img_dilation)
    # cv2.waitKey(0)
    # edges = cv2.Canny(img_dilation,50,100)
    # kernel = np.ones((3,1), np.uint8)
    # edgesDilated = cv2.dilate(edges,kernel, iterations=3)
    # cv2.imshow('edges', edgesDilated)
    # cv2.waitKey(0)
    # output = cv2.connectedComponentsWithStats(edges, 4, cv2.CV_32S)
    # cv2.imshow('lineThresh',thresh)
    # cv2.waitKey(0)
    # cv2.imshow('DilatedThresh',img_dilation)
    # cv2.waitKey(0)
    # cv2.imshow('edges',edges)
    # cv2.waitKey(0)
    # cv2.imshow('Edges dilated',edgesDilated)
    # cv2.waitKey(0)

    img_contours = img_dilation.copy()
    ctrs, hier = cv2.findContours(img_contours, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    sortedCtrs, BBoxs = sort_contours(ctrs,"right-to-left")
    copyOriginalImage = image.copy()
    lineImageCopy = lineImage.copy()
    # print 'Start'
    for i, ctr in enumerate(sortedCtrs):
        # Get bounding box
        x, y, w, h = cv2.boundingRect(ctr)
        startY = y
        endY = y+h
        img2 = lineImage[startY:endY,x-int(0.05*w):x+w]
        # cv2.imshow('word', img2)
        # cv2.waitKey(0)
        aspectRatio = 1.0*w/h
        # if(w < 5 or h < 10 or w*h < 5000):
        #     continue;
        wordsInLine.append(img2)
        #cv2.rectangle(lineImageCopy,(x-int(0.1*w),startY),( x + w, endY),(90,0,255),0)
    # print 'End'
    # cv2.imshow('Words in the line',lineImageCopy)
    # cv2.waitKey(0)
    # cv2.destroyWindow('Words in the line')

testImagesPaths = getImageLocations()
testImagesPaths.sort() #to access the pages with order
wordCount = 0
sumRows = 0
sumColumns = 0
for i,path in enumerate(testImagesPaths):
    imageLocation = path
    orgImage,thresholdImage = getThresholdImage(imageLocation)
    temp = cv2.resize(orgImage, (960, 700))
    # cv2.imshow('Original image',temp)
    # cv2.waitKey(0)
    dilatedImage = getDilatedImage(thresholdImage)
    linesInImage = []
    getLinesInImage(dilatedImage,orgImage,linesInImage)
    # temp = cv2.resize(dilatedImage, (960, 700))
    # cv2.imshow('dilated', temp)
    # cv2.waitKey(0)
    # cv2.imshow('Check image',orgImage)
    # cv2.waitKey(0)
    for i,line in enumerate(linesInImage):
        wordsInLine = []
        temp = cv2.resize(line, (960, 700))
        # cv2.imshow('line', line)
        # cv2.waitKey(0)
        getWordsInLine(line,orgImage,wordsInLine)
        wordCount = 0
        for j, word in enumerate(wordsInLine):
            if len(word) == 0 or len(word[0]) == 0:
                continue
            resizedWord = cv2.resize(word,(416,320))
            cv2.imwrite("./Segmentation/Dataset/Words/word_" + str(wordCount) + ".JPG", word);
            wordCount += 1
            # sumRows += len(word)
            # sumColumns += len(word[0])
            # cv2.imshow('word',resizedWord)
            # cv2.waitKey(0)
        # print wordCount

# print 'Average row is',sumRows/wordCount
# print 'Average col is',sumColumns/wordCount
# the results optained by iterations = 3 and 5x5 kernel is optimum given that
# the next step will be required to identify text from non text and lines from non lines
#find contours
# img_contours = img_dilation.copy()
# ctrs, hier = cv2.findContours(img_contours, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# for i, ctr in enumerate(ctrs):
#     # Get bounding box
#     x, y, w, h = cv2.boundingRect(ctr)
#     #We decrease the height of the contour because the dilates increases it
#     cv2.rectangle(image,(x,y-int(h*0.5)),( x + w, y + h-int(0.05*h)),(90,0,255),2)
#
# # cv2.drawContours(image, ctrs, -1, (0,0,255), 3)
# cv2.imshow('contours',image)
# cv2.waitKey(0)
# #sort contours
# sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
# # def getBoundingBox(ctr):
# #     x,y,w,h = cv2.boundingRect(ctr)
# #     return x,y,w,h
#
# # def getBoundingBoxDimList(ctr):
# #     x = list()
# #     y = list()
# #     w = list()
# #     h = list()
# #     for i,cntr in enumerate(ctr):
# #         tempx,tempy,tempw,temph = getBoundingBox(cntr)
# #         x.append(tempx)
# #         y.append(tempy)
# #         w.append(tempw)
# #         h.append(temph)
#
# #     return x,y,w,h
#
# # x,y,w,h = getBoundingBoxDimList(sorted_ctrs)
# # fig4, ax1 = plt.subplots()
# # bph = ax1.boxplot(h, notch=0, sym='+', vert=0, whis=1.5)
# # plt.show()
# # for flier in bph['fliers']:
#
#     # print(flier)
# # plt.setp(bp['boxes'], color='black')
# # plt.setp(bp['whiskers'], color='black')
# # plt.setp(bp['fliers'], color='red', marker='+')
#
# # print(w,h)
# # h = np.array(h)
# # w = np.array(w)
# # aspect_ratio = h/w
# # print(aspect_ratio)
# # print(h)
# # print(np.average(h))
# extraPadding = 1.0
# print(len(sorted_ctrs))
#
# # print (len(i))
# roi_container=list()
# for i, ctr in enumerate(sorted_ctrs):
#     # Get bounding box
#     x, y, w, h = cv2.boundingRect(ctr)
#
#     # increase dimension with 10%
#     # h = int(np.ceil(h*extraPadding))
#     # w = int(np.ceil(w*extraPadding))
#
#     # yFinal =   image.shape[1]  if(y+h> image.shape[1])else y+h
#     # xFinal =  image.shape[0] if(x+w> image.shape[0]) else x+w
#
#     # print(y-h,x-w)
#     # print ((y -int(h* (extraPadding-1)) < 0), (x -int(w* (extraPadding-1)) < 0) )
#     # yInitial =   0 if (y -int(h* (extraPadding-1)) < 0)else y -int(h* (extraPadding-1))
#     # xInitial = 0 if (x -int(w* (extraPadding-1)) < 0) else x - int(w* (extraPadding-1))
#
#     # print("second",yInitial, xInitial)
#     # Getting ROI
#     # roi = image[yInitial:yFinal, xInitial:xFinal].copy()
#     roi = image[y:y+h, x:x+w]
#     # temp = np.ndarray(roi.shape,dtype=np.array)
#     roi_container.append(roi.copy())
#     # roi = np.append(roi,image[y:y + h, x:x + w].copy())
#     # print(roi)
#     # aspect_ratio = h/w
#     # if(aspect_ratio > 2):
#     # show ROI
#     # cv2.imshow('segment no:'+str(i),roi)
#     cv2.rectangle(image,(x,y),( x + w, y + h ),(90,0,255),2)
#     # cv2.rectangle(image,(xInitial,yInitial),( xFinal, yFinal ),(90,0,255),2)
#     # print(x,y,w,h)
#     # cv2.waitKey(0)
# # print(np.array(roi_container))
#
# def verticalProjection(im):
#     # make sure the image is black and whit or grey scale
#     im = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
#     # create a variable that accummulates the results of each vertical column
#     accumulator = list()
#     # for each column sum all the cells ie from row 0 to row #of rows
#     print(im.shape)
#     for i in range(im.shape[1]):
#         temp_sum = 0
#         for j in range(im.shape[0]):
#             temp_sum = temp_sum + im[j,i]
#
#         accumulator.append(temp_sum)
# #     print(len(accumulator))
#     min_value = min(accumulator)
#     max_value = max(accumulator)
#
#     mid_value = int(((max_value - min_value)*2)/3)
#
#     # bool_accum = [i < mid_value for i in accumulator ]
#     plt.plot(accumulator)
#     plt.show()
#
# def wordSegmentationV2(im):
#     # cv2.
#     # im2,ctrs, hier = cv2.findContours(im.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#
#     # #sort contours
#     # sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
#     # for cntr in sorted_ctrs:
#     #     x, y, w, h = cv2.boundingRect(cntr)
#     #     cv2.rectangle(im,(x,y),( x + w, y + h ),(255,255,255),2)
#
#     # rect = cv2.minAreaRect(ctrs)
#     # box = cv2.boxPoints(rect)
#     # box = np.int0(box)
#     # cv2.drawContours(im,[box],0,(0,0,255),2)
#
#     # kernel = np.ones((0,im.shape[1]),np.int8)
#     # img_dilation = cv2.dilate(im,kernel)
#     img_dilation = im
#
#     cv2.imshow('dilated', img_dilation)
#     cv2.waitKey(0)
#     # image, contours, hierarchy = cv2.findContours(img_dilation,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#     # img_dilation = cv2.cvtColor(img_dilation, cv2.COLOR_RGB2GRAY)
#     # img_dilation = cv2.convertScaleAbs(img_dilation)
#     # im2,ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#
#     # rect = cv2.minAreaRect(ctrs)
#     # box = cv2.boxPoints(rect)
#     # box = np.int0(box)
#     # cv2.drawContours(im,[box],0,(0,0,255),2)
#
#     # detector = cv2.SimpleBlobDetector()
#
#     #Detect blobs.
#     # keypoints = detector.detect(img_dilation)
#
#     print('reached here yeaah')
#     #sort contours
#     sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
#
#
#     for i, ctr in enumerate(sorted_ctrs):
#         # Get bounding box
#         x, y, w, h = cv2.boundingRect(ctr)
#
#         # roi = image[y:y+h, x:x+w]
#
#         cv2.rectangle(im,(x,y),( x + w, y + h ),(0,255,0),2)
#
#     cv2.imshow('show countours',im)
#     cv2.waitKey(0)
#
#
#
#     cv2.imshow("result.png", im)
#     cv2.waitKey(0)
#
# # ret,sub_image_thresh = cv2.threshold(np.array(roi_container[15]),127,255,cv2.THRESH_BINARY_INV)
# # verticalProjection(sub_image_thresh)
# # cv2.imshow('test image show',sub_image_thresh)
# # cv2.waitKey(0)
#
# # wordSegmentationV2(sub_image_thresh)
#
# for i in range(len(roi_container)):
#     sub_image = np.array(roi_container[i])
#     # print(sub_image.shape)
#     if sub_image.shape[0] == 0:
#         continue
#     # print(cv2.reduce(sub_image,1,cv2.REDUCE_SUM,cv2.CV_32S))
#     # ret,sub_image_thresh = cv2.threshold(gray,127,255,cv2.THRESH_BINARY_INV)
#     verticalProjection(sub_image)
#     cv2.imshow('segment no: '+str(i),sub_image)
#     cv2.waitKey(0)
#     # plt.
# # cv2.imshow('marked areas',image)
# # cv2.waitKey(0)

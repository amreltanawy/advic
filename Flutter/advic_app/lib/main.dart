import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

//Pages
import 'add_main.dart';
import 'login.dart';
import 'upload_main.dart';
import 'gallery.dart';
import 'approve.dart';


String token;
String type;
String email;
String imageUrl;


class MainPage extends StatelessWidget {
  // This widget is the root of your application.

  MainPage(String token1, String _type, String _email){
    token = token1;
    type = _type;
    email = _email;
    imageUrl = 'https://placeimg.com/300/300/any';
  }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(token, type),

    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage(String token1, String _type){
    token = token1;
    type = _type;
  }
  @override
  _MyHomePageState createState() => new _MyHomePageState(token, type);
}

class _MyHomePageState extends State<MyHomePage> {


  _MyHomePageState(String token1, String _type){
    token = token1;
    type = _type;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.all(16.0),
        decoration: new BoxDecoration(

          image: new DecorationImage(
              image: new AssetImage("assets/login_background2.jpg"),
              fit: BoxFit.cover),
        ),
        child: new Text('Welcome!                              Swipe left to start',
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
                color: Colors.white
            )
        ),
      ),
      appBar: new AppBar(title: new Text('Main Page')),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text(email),
              accountEmail: new Text(type),
              currentAccountPicture: new CircleAvatar(
                  backgroundImage: new NetworkImage(imageUrl),
              ),
            ),

//            new ListTile(
//              title: new Text('Capture'),
//              trailing: new Icon(Icons.camera_alt),
////              onTap: () {
////                Navigator.of(context).pop();
////                Navigator.push(
////                    context,
////                    new MaterialPageRoute(
////                        builder: (BuildContext context) => new ScanImage()));
////              },
//            ),
            type == "uploader" ? new ListTile(
              title: new Text('Upload'),
              trailing: new Icon(Icons.file_upload),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new FilePickerDemo(token, 'uploader')));
              },
            ) : type == "admin"?

            new ListTile(
              title: new Text('Add'),
              trailing: new Icon(Icons.add),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new AddMain(token, 'administrator')));
              },
            )  :
            new ListTile(
              title: new Text('Check Approvals'),
              trailing: new Icon(Icons.spellcheck),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new ApproveFile(token, "approver")));
              },
            ),
            new ListTile(
              title: new Text('Gallery'),
              trailing: new Icon(Icons.filter),
              onTap: () {
                //Navigator.of(context).pop();
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new Gallery()));
              },
            ),
//            new ListTile(
//              title: new Text('Settings'),
//              trailing: new Icon(Icons.settings),
////              onTap: () {
////                Navigator.of(context).pop();
////                Navigator.push(
////                    context,
////                    new MaterialPageRoute(
////                        builder: (BuildContext context) => new AddUser()));
////              },
//            ),
            new ListTile(
              title: new Text('Signout'),
              trailing: new Icon(Icons.exit_to_app),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new MyApp()));
              },
            ),
          ],
        ),
      ),
    );
  }


}
import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

enum FileType {
  ANY,
  IMAGE,
  CUSTOM,
}

class FilePicker {
  static const MethodChannel _channel = const MethodChannel('file_picker');
  static const String _tag = 'FilePicker';

  FilePicker._();


  static Future<Map<String, String>> getMultiFilePath({FileType type = FileType.ANY, String fileExtension}) async =>
      await _getPath(_handleType(type, fileExtension), true);


  static Future<String> getFilePath({FileType type = FileType.ANY, String fileExtension}) async =>
      await _getPath(_handleType(type, fileExtension), false);


  static Future<File> getFile({FileType type = FileType.ANY, String fileExtension}) async {
    final String filePath = await _getPath(_handleType(type, fileExtension), false);
    return filePath != null ? File(filePath) : null;
  }

  static Future<dynamic> _getPath(String type, bool multipleSelection) async {
    try {
      dynamic result = await _channel.invokeMethod(type, multipleSelection);
      if (result != null && multipleSelection) {
        if (result is String) {
          result = [result];
        }
        return Map<String, String>.fromIterable(result, key: (path) => path.split('/').last, value: (path) => path);
      }
      return result;
    } on PlatformException catch (e) {
      print('[$_tag] Platform exception: ' + e.toString());
    } catch (e) {
      print('[$_tag] Unsupported operation. Method not found. The exception thrown was: ' + e.toString());
    }
    return null;
  }

  static String _handleType(FileType type, String fileExtension) {
    switch (type) {
      case FileType.IMAGE:
        return 'IMAGE';
      case FileType.ANY:
        return 'ANY';
      case FileType.CUSTOM:
        return '__CUSTOM_' + (fileExtension ?? '');
      default:
        return 'ANY';
    }
  }
}
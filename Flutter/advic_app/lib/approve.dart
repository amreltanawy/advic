import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:convert/convert.dart';
import 'dart:convert';
import 'dart:async';

import 'viewPDF.dart';

String token;
String type;
var data;

List<String> Names1 = ['Nothing yet'];
List<String> FileNames1 = ['Nothing yet'];


List<String> Names2 = ['Nothing yet'];
List<String> FileNames2 = ['Nothing yet'];

List<String> Names3 = ['Nothing yet'];
List<String> FileNames3= ['Nothing yet'];

class ApproveFile extends StatefulWidget {

  ApproveFile(String token1, String _type){
    token = token1;
    type = _type;
  }

  @override
  _ApproveFileState createState() => new _ApproveFileState(token,type);
}

class _ApproveFileState extends State<ApproveFile> {

  var woo;

  _ApproveFileState(String token1, String _type){
    token = token1;
    type = _type;
    woo = getData2();
    //Names.clear();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(
        title: const Text('View Files'),
        automaticallyImplyLeading: true,
      ),

      body: new Center(
          child: new Padding(
            padding: const EdgeInsets.only(top: 0.1, left: 0.1, right: 0.1),
            child: new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(16.0),
              decoration: new BoxDecoration(

                image: new DecorationImage(
                    image: new AssetImage("assets/login_background2.jpg"),
                    fit: BoxFit.cover),
              ),
              child: new Center(
                child: new Column(
                  children: <Widget>[
                    new Text(
                        'Not Verified',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                          color: Colors.lightBlue[900],
                        )
                    ),
                    new Expanded(
                      child: new ListView.builder(
                        reverse: false,
                        itemBuilder: (_,int index)=>EachList1(index),
                        itemCount: Names1.length,
                      ),
                    ),
                    new Text(
                        'Verified',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                          color: Colors.lightBlue[900] ,
                        )
                    ),
                    new Expanded(
                      child: new ListView.builder(
                        reverse: false,
                        itemBuilder: (_,int index)=>EachList2(index),
                        itemCount: Names2.length,
                      ),
                    ),

                    new Text(
                        'Pending',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                          color: Colors.lightBlue[900] ,
                        )
                    ),
                    new Expanded(
                      child: new ListView.builder(
                        reverse: false,
                        itemBuilder: (_,int index)=>EachList3(index),
                        itemCount: Names3.length,
                      ),
                    ),

                  ],
                ),
              ),
            ),
          )),
    );

  }

  Future<String> getData2() async {
    var response = await http.get(
        Uri.encodeFull("http://138.68.242.54/files/approver/files"),
        headers: {
          "Content-Type": "application/json",
          "token": token
        }
    );
    //print("RESPONSE " + response.body);
    data = json.decode(response.body);
    Names1.clear();
    FileNames1.clear();
    Names2.clear();
    FileNames2.clear();
    Names3.clear();
    FileNames3.clear();
    for(int i=0;i<data.length;i++)
    {
      if(data[i]['file_status']== 'Not Verified')
      {
        Names1.add(data[i]['upload_name']);
        FileNames1.add(data[i]['pdfFile_name']);
      }
      else if(data[i]['file_status']== 'Verified')
      {
        Names2.add(data[i]['upload_name']);
        FileNames2.add(data[i]['pdfFile_name']);
      }
      if(data[i]['file_status']== 'Pending')
      {
        Names3.add(data[i]['upload_name']);
        FileNames3.add(data[i]['pdfFile_name']);
      }
    }
    return null;
  }

}

class EachList1 extends StatelessWidget{
  final int index;
  EachList1(this.index);
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext context) => new ViewPDF(token, type, Names1[this.index], FileNames1[this.index])));
        },
        child: new Card(
          child: new Container(
            padding: EdgeInsets.all(8.0),
            child: new Row(
              children: <Widget>[
                new CircleAvatar(child: new Text(Names1[index][0])),
                new Padding(padding: EdgeInsets.only(right: 10.0)),
                new Text(Names1[index],style: TextStyle(fontSize: 20.0)),

              ],
            ),
          ),
        )
    );

  }

}

class EachList2 extends StatelessWidget{
  final int index;
  EachList2(this.index);
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext context) => new ViewPDF(token, type, Names2[this.index], FileNames2[this.index])));
        },
        child: new Card(
          child: new Container(
            padding: EdgeInsets.all(8.0),
            child: new Row(
              children: <Widget>[
                new CircleAvatar(child: new Text(Names2[index][0])),
                new Padding(padding: EdgeInsets.only(right: 10.0)),
                new Text(Names2[index],style: TextStyle(fontSize: 20.0)),

              ],
            ),
          ),
        )
    );

  }

}

class EachList3 extends StatelessWidget{
  final int index;
  EachList3(this.index);
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext context) => new ViewPDF(token, type, Names3[this.index], FileNames3[this.index])));
        },
        child: new Card(
          child: new Container(
            padding: EdgeInsets.all(8.0),
            child: new Row(
              children: <Widget>[
                new CircleAvatar(child: new Text(Names3[index][0])),
                new Padding(padding: EdgeInsets.only(right: 10.0)),
                new Text(Names3[index],style: TextStyle(fontSize: 20.0)),

              ],
            ),
          ),
        )
    );

  }

}
//import 'dart:async';
//import 'dart:io';
//import 'package:image_picker/image_picker.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//
//
//class ScanImage extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() => new _ScanImage();
//}
//
//class _ScanImage extends State<ScanImage> {
//
//  File _image;
//
//  Future getImage() async{
//    var image = await ImagePicker.pickImage(source: ImageSource.camera);
//    setState(() {
//      _image = image;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'Scanner/Uploader',
//      home: new Scaffold(
//        appBar: new AppBar(title: new Text('Image Scanner')
//        ),
//        body: new Center(
//          child: _image == null? new Text('No Image Selected'): new Image.file(_image),
//        ),
//        floatingActionButton: new FloatingActionButton(onPressed: getImage, tooltip: 'Pick Image',
//        child: new Icon(Icons.camera),),
//      ),
//    );
//  }
//}
import 'package:flutter/material.dart';

import 'addUser.dart';
import 'addSite.dart';
import 'main.dart';

String token;
String type;

class AddMain extends StatelessWidget {
  // This widget is the root of your application.

  AddMain(String token1, String _type){
    token = token1;
    type = _type;
  }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new AddMainPage(token, type),
    );
  }
}

class AddMainPage extends StatefulWidget {

  AddMainPage(String token1, String _type){
    token = token1;
    type = _type;
  }
  @override
  _AddMainPageState createState() => new _AddMainPageState(token, type);
}

class _AddMainPageState extends State<AddMainPage> {
  void hop()
  {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new MainPage(token,"admin","admintest@admin.com")));
  }
  _AddMainPageState(String token1,String _type){
    token = token1;
    type = _type;
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: AppBar(
        title: new Text('Add'),
        leading: IconButton(icon: Icon(Icons.arrow_back,color: Colors.white,),
            onPressed: hop),),

      body: new Container(
        padding: EdgeInsets.all(16.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildButtonUser(),
            _buildButtonSite()
          ],
        ),
      ),
    );

  }

  Widget _buildButtonUser() {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Add User'),
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.blue[600],
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: _loginPressed1,
          ),
        ],
      ),
    );

  }

  Widget _buildButtonSite() {
    return new Container(

      child:
      new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Add Site'),
            textColor: Colors.white,
            color: Colors.blue[600],
            padding: const EdgeInsets.all(8.0),
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: _loginPressed2,
          ),

        ],
      ),
    );

  }

  void _loginPressed1()
  {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new AddUser(token,type)));
  }

  void _loginPressed2()
  {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new AddSite(token,type)));
  }

}
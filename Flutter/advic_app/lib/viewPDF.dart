import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';

//import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
//import 'package:flutter_full_pdf_viewer/full_pdf_viewer_plugin.dart';
//import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';

String token;
String type;
String ok = "OK";
String nok = "UNIDENTIFIED";
var data;
var upload_name, pdf_name ;

class ViewPDF extends StatefulWidget {

  ViewPDF(String token1, String _type, String _upload_name, String _pdf_name){
    token = token1;
    type = _type;
    upload_name = _upload_name;
    pdf_name = _pdf_name;
  }

  @override
  _ViewPDFState createState() => new _ViewPDFState(token,type, upload_name, pdf_name);
}

class _ViewPDFState extends State<ViewPDF> {

  var woo;

  _ViewPDFState(String token1, String _type, String _upload_name, String _pdf_name){
    token = token1;
    type = _type;
    upload_name = _upload_name;
    pdf_name = _pdf_name;
    //woo = getData3();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(
        title: const Text('Approve Files'),
        automaticallyImplyLeading: true,
      ),

      body: new Center(
          child: new Padding(
            padding: const EdgeInsets.only(top: 0.1, left: 0.1, right: 0.1),
            child:
            new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(16.0),
              decoration: new BoxDecoration(

                image: new DecorationImage(
                    image: new AssetImage("assets/login_background2.jpg"),
                    fit: BoxFit.fill),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text(
                      "Materials: $ok \n-----------\n\n"
                          "Made in Statement: $nok \n-----------\n\n"
                          "Caution Messages: $ok \n-----------\n\n"
                          "Ingredients: $ok \n-----------\n\n"
                          "Expiration Date: $ok \n-----------\n\n"
                      ,
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15.0,
                        color: Colors.blue[600],
                      )
                  ),
                  _buildButtonApprove(),
                  _buildButtonNotApprove(),
                  _buildButtonDownload(),
                ],
              ),
            ),


          )
      ),
    );

  }

  Widget _buildButtonApprove() {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Approve'),
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.blue[600],
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: getData2,
          ),
        ],
      ),
    );

  }

  Widget _buildButtonNotApprove() {
    return new Container(

      child:
      new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Not Approve'),
            textColor: Colors.white,
            color: Colors.blue[600],
            padding: const EdgeInsets.all(8.0),
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: getData2NOTAPPROVE,
          ),

        ],
      ),
    );

  }

  Widget _buildButtonDownload() {
    return new Container(

      child:
      new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Download File'),
            textColor: Colors.white,
            color: Colors.blue[600],
            padding: const EdgeInsets.all(8.0),
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: getData3,
          ),

        ],
      ),
    );

  }

  Future<String> getData2() async {
    print("upload_name " + upload_name);
    print("pdf_name " + pdf_name);

    var response = await http.patch(
        Uri.encodeFull("http://138.68.242.54/files/approver/verify/file/"+upload_name),
        headers: {
          //"Content-Type": "application/json",
          "token": token
        },
        body: {
          "newStatus" : "Verified"
        }


    );
    Fluttertoast.showToast(
        msg: "Changed Status: Approved",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        fontSize: 16.0
    );


    print("I WON'T HESITATE");
    return null;
  }

}

Future<String> getData2NOTAPPROVE() async {
  print("upload_name " + upload_name);
  print("pdf_name " + pdf_name);

  var response = await http.patch(
      Uri.encodeFull("http://138.68.242.54/files/approver/verify/file/"+upload_name),
      headers: {
        //"Content-Type": "application/json",
        "token": token
      },
      body: {
        "newStatus" : "Not Verified"
      }
  );

  Fluttertoast.showToast(
      msg: "Changed Status: NOT Approved",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.blue,
      textColor: Colors.white,
      fontSize: 16.0
  );
  print("I WON'T HESITATE");
  return null;
}



Future<String> getData3() async {

  Directory appDocDirectory = await getExternalStorageDirectory();// getApplicationDocumentsDirectory();
  print("SAVED PATH "+appDocDirectory.path);
  HttpClient client = new HttpClient();
  var _downloadData = List<int>();
  var fileSave = new File(appDocDirectory.path+'/Documents/'+pdf_name);
  client.getUrl(Uri.parse("http://138.68.242.54/files/approver/file/"+upload_name+"/"+pdf_name))
      .then((HttpClientRequest request) {
    request.headers.add("application", "json");
    request.headers.add("token",token);
    return request.close();
  })
      .then((HttpClientResponse response) {
    response.listen((d) => _downloadData.addAll(d),
        onDone: () {
          fileSave.writeAsBytes(_downloadData);
        }
    );
    print("RESPONSE "+ response.headers.toString());

    Fluttertoast.showToast(
        msg: "Downloaded Successfully",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        fontSize: 16.0
    );
  });
}
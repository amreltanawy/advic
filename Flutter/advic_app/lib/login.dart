import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import'main.dart';

void main() => runApp(new MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Simple Login Demo',
      theme: new ThemeData(
          primarySwatch: Colors.blue
      ),
      home: new LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType {
  login
}

class _LoginPageState extends State<LoginPage> {

  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  String _email = "";
  String _password = "";
  FormType _form = FormType.login; // our default setting is to login, and we should switch to creating an account when the user chooses to

  var token;
  var type;


  _LoginPageState() {
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  // Swap in between our two forms, registering and logging in
  void _formChange () async {
    setState(() {
      if (_form == FormType.login) {
        _form = FormType.login;
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    var logo = new Container(
      width: 160.0,
      height: 160.0,
      margin: const EdgeInsets.only(top: 50.0, bottom:80.0),
      alignment: Alignment.center,
      decoration: new BoxDecoration(

        image: DecorationImage(
            image: AssetImage('assets/logo1.png'),
            fit: BoxFit.fill
        ),
      ),
    );

    return new Scaffold(
        resizeToAvoidBottomInset: false,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/login_background2.jpg"),
              fit: BoxFit.cover),
        ),
        padding: EdgeInsets.all(16.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              logo,
              _buildTextFields(),
              _buildButtons(),

            ],
          ),
        ),
      ),
    );
  }



  Widget _buildTextFields() {
    return new Container(

      child: new Column(

        children: <Widget>[
          new Container(
            child: new TextField(
              controller: _emailFilter,
              decoration: new InputDecoration(
                  labelText: 'Email'
              ),

            ),
          ),
          new Container(
            child: new TextField(
              controller: _passwordFilter,
              decoration: new InputDecoration(
                  labelText: 'Password'
              ),

              obscureText: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new RaisedButton(
              child: new Text('Login'),
              textColor: Colors.white,
              color: Colors.indigoAccent.shade100.withOpacity(0.8),
              onPressed: _loginPressed,
            ),
          ],
        ),
      );
    }
  }


  // These functions can self contain any user auth logic required, they all have access to _email and _password

  void _loginPressed () {
    print('The user wants to login with $_email and $_password');
    getData();
  }

  Future<String> getData() async {

    var body1 = json.encode({
      "email":_email,
      "password":_password
    });
    var response = await http.post(
        Uri.encodeFull("http://138.68.242.54/user/login"),
        headers: {
          "Content-Type": "application/json"
        },
        body: body1

    );
    var data = json.decode(response.body);
    if(data['message']== "Auth successful") {
      token = data['token'];
      type = data['user_type'];

      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) => new MainPage(token, type, _email)));
      return "Success";
    }
    else {
      print(data['message']);
      print("HELLOOOOOOOOOOO");
    }

  }
}
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:convert/convert.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'upload_file.dart';
import 'main.dart';

String token;
String type;
File _path1file;
File _path2file;
String _path1;
String _path2;

class FilePickerDemo extends StatefulWidget {

  FilePickerDemo(String token1, String _type){
    token = token1;
    type = _type;
  }

  @override
  _FilePickerDemoState createState() => new _FilePickerDemoState(token,type);
}

class _FilePickerDemoState extends State<FilePickerDemo> {

  final TextEditingController _uploadNameFilter = new TextEditingController();
  final TextEditingController _pdfNameFilter = new TextEditingController();
  final TextEditingController _excelNameFilter = new TextEditingController();

  String _uploadName = "";
  String _pdfName = "";
  String _excelName = "";
  String _fileName;
  String _path;

  _FilePickerDemoState(String token1, String _type){
    token = token1;
    type = _type;

    _uploadNameFilter.addListener(_uploadListen);
    _pdfNameFilter.addListener(_pdfListen);
    _excelNameFilter.addListener(_excellListen);

  }
  void _uploadListen() {
    if (_uploadNameFilter.text.isEmpty) {
      _uploadName = "";
    } else {
      _uploadName = _uploadNameFilter.text;
    }
  }
  void _pdfListen() {
    if (_pdfNameFilter.text.isEmpty) {
      _pdfName = "";
    } else {
      _pdfName = _pdfNameFilter.text;
    }
  }
  void _excellListen() {
    if (_excelNameFilter.text.isEmpty) {
      _excelName = "";
    } else {
      _excelName = _excelNameFilter.text;
    }
  }
//  @override
//  void initState() {
//    super.initState();
//    _controller.addListener(() => _extension = _controller.text);
//  }

  void _openFileExplorer(String typee) async {
    if(typee == "pdf") {
      try {
        _path1file = await FilePicker.getFile(type: FileType.CUSTOM, fileExtension: typee);
        _path1 = _path1file.path;
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }
    }
    else if(typee == "xlsx")
      {
        try {
          _path2file = await FilePicker.getFile(type: FileType.CUSTOM, fileExtension: typee);
          _path2 = _path2file.path;
        } on PlatformException catch (e) {
          print("Unsupported operation" + e.toString());
        }
      }

      if (!mounted) return;

      setState(() {
        if(_path!=null) _fileName =  _path.split('/').last;
      });

    //viewText(_path1);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: new AppBar(
        title: const Text('Upload Files'),
        automaticallyImplyLeading: true,
      ),

      body: new Center(
          child: new Padding(
            padding: const EdgeInsets.only(top: 0.1, left: 0.1, right: 0.1),
            child: new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(16.0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("assets/login_background2.jpg"),
                    fit: BoxFit.cover),
              ),

              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  new Container(
                    child: new TextField(
                      controller: _uploadNameFilter,
                      decoration: new InputDecoration(
                          labelText: 'Upload Name'
                      ),

                    ),
                  ),

                  new Container(
                    child: new TextField(
                      controller: _pdfNameFilter,
                      decoration: new InputDecoration(
                          labelText: 'Pdf Name'
                      ),

                    ),
                  ),

                  new Container(
                    child: new TextField(
                      controller: _excelNameFilter,
                      decoration: new InputDecoration(
                          labelText: 'Excel Name'
                      ),

                    ),
                  ),


                  new Padding(
                    padding: const EdgeInsets.only(top: 50.0, bottom: 20.0),
                    child: new RaisedButton(
                      child: new Text('Choose Pdf File'),
                      color: Colors.blue,
                      textColor: Colors.white,
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      onPressed: () => _openFileExplorer("pdf"),
                    ),
                  ),


                  new Padding(
                    padding: const EdgeInsets.only(top: 50.0, bottom: 20.0),
                    child: new RaisedButton(
                      child: new Text('Choose Excel File'),
                      color: Colors.blue,
                      textColor: Colors.white,
                      shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      onPressed: () => _openFileExplorer("xlsx"),
                    )
                  ),

                  new Padding(
                      padding: const EdgeInsets.only(top: 50.0, bottom: 20.0),
                      child: new RaisedButton(
                        child: new Text('Upload'),
                        color: Colors.blue,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                        onPressed: () => getData2(),
                      )
                  ),
                  new Text(
                    _path1 != null ?
                    "Pdf/Image: $_path1 \n-----------\n"
                        "Excel: $_path2":
                    " ",
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.white,
                      )
                  )
                ],
              ),
            ),
          )),
    );

  }



  void viewText()
  {
    print("BUT I AM NOT NULL LOOK ");
    _path1 = _path1file.path;
    _path2 = _path2file.path;

    new Text(
        "Pdf/Image: $_path1 "
            "Excel: $_path2",
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30.0,
            color: Colors.blueGrey[700],
        )
    );
  }

  Future<String> getData2() async {

    List<int> imageBytes = await _path1file.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);

    setState(() {
      _path1 = _path1;
      _path2 = _path2;
    });
    var body1 = json.encode({
      "pdf_file": base64Encode(await _path1file.readAsBytesSync()),
      "pdf_name": _pdfName,
      "excel_name": _excelName,
      "upload_name": _uploadName,
      "excel_file": base64Encode(await _path2file.readAsBytesSync())
    });
    print("BODY IS " + body1);
    var response = await http.post(
        Uri.encodeFull("http://138.68.242.54/file/addfile"),
        headers: {
          "token": token,
          "Content-Type": "application/json"
        },
        body: body1
    );
    print("RESPONSE " + response.body);
    var data = json.decode(response.body);
    if (data['message'] == "File Created") {
      Navigator.of(context).pop();
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) =>
              new MainPage(token, type, "")));
      return "Success";
    }
    else {
      print(data['error']);
      return "Fail";
    }
  }

}
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';


String token;
String type;
class AddSite extends StatefulWidget {


  AddSite(String token1,String _type){
    token = token1;
    type = _type;
  }

  @override
  _AddSiteState createState() => new _AddSiteState(token, type);
}

class _AddSiteState extends State<AddSite> {


  _AddSiteState(token1,String _type) {

    token = token1;
    type = _type;

    _nameFilter.addListener(_nameListen);
    _locationFilter.addListener(_locationListen);
  }

  final TextEditingController _nameFilter = new TextEditingController();
  final TextEditingController _locationFilter = new TextEditingController();

  var _name = '';
  var _location = '';

  void _nameListen() {
    if (_nameFilter.text.isEmpty) {
      _name = "";
    } else {
      _name = _nameFilter.text;
    }
  }

  void _locationListen() {
    if (_locationFilter.text.isEmpty) {
      _location = "";
    } else {
      _location = _locationFilter.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: _buildBar(context),
      body: _buildTextFields(),
    );
  }

  Widget _buildBar(BuildContext context) {

    return new AppBar(
      title: new Text("Add Site"),
      automaticallyImplyLeading: true,
      centerTitle: true,
    );
  }


  Widget _buildTextFields() {
    return new Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
            colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.dstATop),
            image: new AssetImage("assets/2.jpg"),
            fit: BoxFit.cover),
      ),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new TextField(
              controller: _nameFilter,
              decoration: new InputDecoration(
                  labelText: 'Site Name'
              ),
            ),
          ),
          new Container(
            child: new TextField(
              controller: _locationFilter,
              decoration: new InputDecoration(
                  labelText: 'Site Location'
              ),
            ),
          ),
          new Container(
              child: _buildButtons()
          )
        ],

      ),

    );
  }

  Widget _buildButtons() {

    return new Container(

      child: new Column(
        children: <Widget>[
          new RaisedButton(
            child: new Text('Add Site'),
            textColor: Colors.white,
            color: Colors.blue,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: _addPressed,
          )
        ],
      ),
    );

  }


// These functions can self contain any user auth logic required, they all have access to _email and _password

  void _addPressed () {
    getData();
    Navigator.of(context).pop();
//    Navigator.push(
//        context,
//        new MaterialPageRoute(
//            builder: (BuildContext context) => new AddMain(token,type)));
  }


  Future<String> getData() async {

    var body1 = json.encode({ 'site_name':_nameFilter.text,
      'site_location':_locationFilter.text});

    print("Body: " + body1);
    var response = await http.post(
        Uri.encodeFull("http://138.68.242.54/site/addsite"),
        headers: {
          "Content-Type": "application/json",
          "token": token
        },
        body: body1
    );
    var data = json.decode(response.body);
    //print (data[0][1]);
    if(data['message']== "Site Created")
    {
      print("SITE CREATED");
      return "Success!";
    }
    else
    {
      //var error = json.decode(data['error'].body);
      print("ERROR" + data['message']);
      return "Fail!";
    }
  }

}
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './main.dart';
import 'add_main.dart';

String token;
int type;
class AddUser extends StatefulWidget {


  AddUser(String token1, _type){
    token = token1;
    type = _type;
  }

  @override
  _AddUserState createState() => new _AddUserState(token,type);
}

class _AddUserState extends State<AddUser> {

  var _type3 = ['Cairo Plant','6 October Plant'];
  var _type2 = ['Baby Care','Home Care', 'Fabric Care','Oral Care','Family Care','Hair Care', 'Grooming'];
  var _type1 = ['uploader','approver'];

  var _currentItemSelected3;
  var _currentItemSelected2;
  var _currentItemSelected1;

  String identifier;

  _AddUserState(token1,_type) {

    token = token1;
    type = _type;

    _currentItemSelected3 = '6 October Plant';
    _currentItemSelected2 = 'Baby Care';
    _currentItemSelected1 = 'uploader';

    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();

  var _email = '';
  var _password = '';

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: _buildBar(context),
      body: _buildTextFields(),
    );
  }


  Widget _buildBar(BuildContext context) {

    return new AppBar(
      title: new Text("Add User"),
      automaticallyImplyLeading: true,
      centerTitle: true,
    );
  }


  Widget _buildTextFields() {

    return new Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new AssetImage("assets/2.jpg"),
            colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.dstATop),
            fit: BoxFit.cover),
      ),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new TextField(
              controller: _emailFilter,
              decoration: new InputDecoration(
                  labelText: 'Email'
              ),
            ),
          ),
          new Container(

            child: new TextField(
              controller: _passwordFilter,
              decoration: new InputDecoration(
                  labelText: 'Password'
              ),
              obscureText: true,
            ),
          ),
          DropdownButton<String>(
            items: _type1.map((String dropDownStringItem){
              return DropdownMenuItem<String>(
                value:dropDownStringItem,
                child: Text(dropDownStringItem),
              );
            }).toList(),

            onChanged: (String newValueSelected){
              setState(() {
                this._currentItemSelected1 = newValueSelected;
              });
            },
            value: _currentItemSelected1,
          ),
          DropdownButton<String>(
            items: _type2.map((String dropDownStringItem){
              return DropdownMenuItem<String>(
                value:dropDownStringItem,
                child: Text(dropDownStringItem),
              );
            }).toList(),

            onChanged: (String newValueSelected){
              setState(() {
                this._currentItemSelected2 = newValueSelected;
              });
            },
            value: _currentItemSelected2,
          ),
          DropdownButton<String>(
            items: _type3.map((String dropDownStringItem){
              return DropdownMenuItem<String>(
                value:dropDownStringItem,
                child: Text(dropDownStringItem),
              );
            }).toList(),

            onChanged: (String newValueSelected){
              setState(() {
                this._currentItemSelected3 = newValueSelected;
              });
            },
            value: _currentItemSelected3,
          ),
          new Container(
              child: _buildButtons()
          )
        ],

      ),

    );
  }

  Widget _buildButtons() {

    return new Container(

      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            child: new Text('Add User'),
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.blue,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            onPressed: _addPressed,
          )
        ],
      ),
    );

  }


// These functions can self contain any user auth logic required, they all have access to _email and _password

  void _addPressed () {

    getData();
    Navigator.of(context).pop();
  }


  Future<String> getData() async {
    getData2();
    print("I am back from getData 2 Helloo theereee ");
    var body1 = json.encode({ 'email':_emailFilter.text,
      'password':_passwordFilter.text,
      'user_type': _currentItemSelected1 ,
      'user_site': identifier});

    print("Body: " + body1);
    var response = await http.post(
        Uri.encodeFull("http://138.68.242.54/user/adduser"),
        headers: {
          "Content-Type": "application/json",
          "token": token
        },
        body: body1
    );
    var data = json.decode(response.body);
    //print (data[0][1]);
    if(data['message']== "User Created")
    {
      print("USER CREATED");
      Fluttertoast.showToast(
          msg: "User is added succesfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0
      );
      return "Success!";
    }
    else
    {
      //var error = json.decode(data['error'].body);
      print("ERROR: " + data['message']);
      return "Fail!";
    }
  }

  Future<String> getData2() async {
    var response = await http.get(
      Uri.encodeFull("http://138.68.242.54/sites"),
      headers: {
        "Content-Type": "application/json",
        "token": token
      },
    );
    var data = json.decode(response.body);

    for(int i=0;i<data.length;i++)
    {
      if( data[i]['site_location'] == _currentItemSelected3 )
        if(data[i]['site_name'] == _currentItemSelected2)
          identifier = data[i]['_id'];
    }

    return null;
  }

}